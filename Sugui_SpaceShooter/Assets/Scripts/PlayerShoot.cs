﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

    public GameObject projectilePrefab;
    public float projectilePrefabSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            GameObject bullet = Instantiate(projectilePrefab, this.transform.position, Quaternion.identity) as GameObject;
            Destroy(bullet, 1.0f);
        }
	}
}
