﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawning : MonoBehaviour {

    public GameObject asteroidPrefab;
    public Vector3 spawnValues;
    public int asteroidCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < asteroidCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                GameObject asteroid = Instantiate(asteroidPrefab, spawnPosition, Quaternion.identity) as GameObject;  
                Destroy(asteroid, 5.0f);

                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }
}
