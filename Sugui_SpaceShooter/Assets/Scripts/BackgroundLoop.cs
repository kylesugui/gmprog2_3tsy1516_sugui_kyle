﻿using UnityEngine;
using System.Collections;

public class BackgroundLoop : MonoBehaviour {

    // Use this for initialization
    [SerializeField] float backgroundSpeed = 5.0f;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Vector3.back * Time.deltaTime * backgroundSpeed);
	}
}
