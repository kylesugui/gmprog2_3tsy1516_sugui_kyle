﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float playerSpeed = 5.0f;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Movement();

    }

    void Movement()
    {
        if (Input.GetKey(KeyCode.UpArrow))
            this.transform.Translate(Vector3.up * Time.deltaTime * playerSpeed);

        if (Input.GetKey(KeyCode.DownArrow))
            this.transform.Translate(Vector3.down * Time.deltaTime * playerSpeed);

        if (Input.GetKey(KeyCode.LeftArrow))
            this.transform.Translate(Vector3.left * Time.deltaTime * playerSpeed);

        if (Input.GetKey(KeyCode.RightArrow))
            this.transform.Translate(Vector3.right * Time.deltaTime * playerSpeed);

        Vector3 Width = new Vector3(5.3f, 0.0f, 0.0f); //min and max screen width
        
    }
}
