﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {

    [SerializeField]float speed = 20;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Vector3.up * speed * Time.deltaTime);
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "Asteroid")
        {
            Debug.Log("hit");
            Destroy(gameObject);
            Destroy(other.gameObject); 
        }
    }
}
