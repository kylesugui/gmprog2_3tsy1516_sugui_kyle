﻿using UnityEngine;
using System.Collections;

public class AsteroidProjectile : MonoBehaviour {

    [SerializeField] float speed = 5;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector3.down * speed * Time.deltaTime);
    }
}
