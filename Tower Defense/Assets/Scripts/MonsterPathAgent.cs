﻿using UnityEngine;
using System.Collections;

public class MonsterPathAgent : MonoBehaviour {

    // Use this for initialization
    
    NavMeshAgent agent;
    public Transform target;

    void Start () {
        target = GameObject.Find("End Point").transform;
        agent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        
        agent.SetDestination(target.position);

    }
}
