﻿using UnityEngine;
using System.Collections;

public class WaveSystem : MonoBehaviour {
    
    public Transform StartPoint;
    public Transform EndPoint;
    public GameObject monster;
    public float beginSpawningPause;
    public float spawnInterval;
    public float waveInterval;

    [SerializeField] int NumberOfMonsters;
    NavMeshAgent agent;
	// Use this for initialization
	void Start () {
        StartCoroutine(SpawnMonsters());
    }

    IEnumerator SpawnMonsters()
    {
        yield return new WaitForSeconds(beginSpawningPause);
        while (true)
        {
            for (int i = 0; i < NumberOfMonsters; i++)
            {
                Instantiate(monster, StartPoint.position, Quaternion.identity);
                yield return new WaitForSeconds(spawnInterval);
            }
            yield return new WaitForSeconds(waveInterval);
        }
    }
}

//IEnumerator SpawnWaves()
//{
//    yield return new WaitForSeconds(startWait);
//    while (true)
//    {
//        for (int i = 0; i < hazardCount; i++)
//        {
//            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
//            Quaternion spawnRotation = Quaternion.identity;
//            Instantiate(hazard, spawnPosition, spawnRotation);
//            yield return new WaitForSeconds(spawnWait);
//        }
//        yield return new WaitForSeconds(waveWait);
//    }
//}