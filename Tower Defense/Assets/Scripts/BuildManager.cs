﻿using UnityEngine;
using System.Collections;

public class BuildManager : MonoBehaviour {

	Ray ray;
	RaycastHit hit;
	//[SerializeField] GameObject prefabTower;
	public GameObject objectTower;

	void Start () {
	}

	void Update () {
		SelectArea();

	}

	public void CreateTower(GameObject prefabTower)
	{
		GameObject twr =  Instantiate (prefabTower) as GameObject;
		twr.name = "TestTower";
		objectTower = twr;
       
    }

	Vector3 SnapToGrid(Vector3 towerObject)
	{
		return new Vector3(Mathf.Round(towerObject.x),
							towerObject.y,
							Mathf.Round(towerObject.z));
	}

	void SelectArea() {

		if (objectTower != null) 
		{	
			ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
			if (Physics.Raycast (ray, out hit)) {
				Vector3 towerPos = hit.point;
				objectTower.transform.position = SnapToGrid(towerPos);
				//Debug.Log (hit.point);

				if (hit.point.y > 1) 
				{
					objectTower.GetComponent<TowerScript> ().Buildable ();

					if (Input.GetMouseButtonDown (0)) 
					{
						objectTower.GetComponent<TowerScript> ().Build ();
						objectTower = null;
					}
				}
				else
					objectTower.GetComponent<TowerScript> ().NonBuildable ();

				Debug.DrawLine (ray.origin, hit.point, Color.red);
			}



		}
	}
}
