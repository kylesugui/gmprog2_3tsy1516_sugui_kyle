﻿using UnityEngine;
using System.Collections;

public class CameraPanning : MonoBehaviour {

    // Get the mouse position
    // Get the camera position
    // Get the edges of the screen
    // If or while mouse position is detected near an edge, the camera moves on a direction where the mouse collided with.

    [SerializeField]
    float cameraPanningSpeed = 25.0f;
    
    [SerializeField]

    public Transform worldCamera;
    //Vector3 cameraPos = Camera.main.WorldToViewportPoint()
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.mousePosition.x >= Screen.width)
            transform.Translate(Vector3.right * Time.deltaTime * cameraPanningSpeed, Space.World);

        if (Input.mousePosition.x <= 0)
            transform.Translate(Vector3.left * Time.deltaTime * cameraPanningSpeed, Space.World);

        if (Input.mousePosition.y >= Screen.height)
            transform.Translate(Vector3.up * Time.deltaTime * cameraPanningSpeed, Space.World);

        if (Input.mousePosition.y <= 0)
            transform.Translate(Vector3.down * Time.deltaTime * cameraPanningSpeed, Space.World);

    }
}
