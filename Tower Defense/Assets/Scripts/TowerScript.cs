﻿using UnityEngine;
using System.Collections;

public class TowerScript : MonoBehaviour {

	[SerializeField]Material towerMat;

    public GameObject tower;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        tower = gameObject.GetComponent<BuildManager>().objectTower;
        Build();
    }

	public void Buildable()
	{
		towerMat.color = Color.green;
	}

	public void NonBuildable()
	{
		towerMat.color = Color.red;
	}

	public void Build()
	{
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(tower, Input.mousePosition, Quaternion.identity);
            Debug.Log("Built");
        }
        towerMat.color = Color.white;
	}
}
