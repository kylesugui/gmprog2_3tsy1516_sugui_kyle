﻿using UnityEngine;
using System.Collections;

public class FSM : MonoBehaviour
{
    public enum FSMStates
    {
        Run,
        Patrol, // AI's default state.
        Attack,
        Chase,
    }

    public FSMStates curState;

    public GameObject DestinationPoint;

    public float scale = 2.0f;
    public float growthIncrease = 0.1f;
    public float enemySpeed = 1.0f;
    float distanceAway = 5.0f; // For Chase state (how much 'near' from the detected object's edge/circumference)

    public EnemyDetector Detector;

    // Min/Max random destination points for AI (patrol state)
    public float RandomPointX;
    public float RandomPointY;

    Vector2 newPoint;
    Vector2 dir;
    Vector3 destPoint;

    void Start()
    {
        curState = FSMStates.Patrol;
        Vector2 position = new Vector3(Random.Range(-RandomPointX, RandomPointX), Random.Range(-RandomPointY, RandomPointY));
        Instantiate(DestinationPoint, position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector2(scale, scale);
        if (Detector.detect == true)
        {
            Debug.Log("Detect = true");
        }

        if (Detector.detect == false)
        {
            Debug.Log("Detect = false");
        }
        if (!Detector.detect)
            curState = FSMStates.Patrol;

        switch (curState)
        {
            case FSMStates.Run: RunState(); break;
            case FSMStates.Patrol: PatrolState(); break;
            case FSMStates.Attack: AttackState(); break;
            case FSMStates.Chase: ChaseState(); break;
        }
    }
    protected void PatrolState()
    {
        Vector3 destPoint = DestinationPoint.transform.position;
        // Check if an object is detected.
        // Generate a random destination Point.
        // Move to that destination point.
        // Generate a new destination point when reached.
        if (Detector.detect)
        {
            EvaluateState();
        }

        dir = destPoint - transform.position;

        // If object's distance between the point is less than 1 unit, the point is relocated.
        if (dir.magnitude <= 1)
        {
            DestinationPoint.transform.position = GenerateDestinationPoint(newPoint);
        }

        Debug.DrawLine(transform.position, DestinationPoint.transform.position, Color.green);
        transform.position = Vector2.MoveTowards(transform.position, DestinationPoint.transform.position, (enemySpeed / scale) * Time.deltaTime);

    }

    protected void AttackState()
    {
        dir = Detector.nearByEnemy.transform.position - transform.position;
        if (dir.magnitude > (Detector.nearByEnemy.transform.localScale.x / 2 + distanceAway))
        {
            curState = FSMStates.Chase;
            Debug.Log("Transition to Chase state!");
        }

        transform.position = Vector2.MoveTowards(transform.position, Detector.nearByEnemy.transform.position, (enemySpeed / scale) * Time.deltaTime);
        Debug.Log("AI is attacking");
        Debug.DrawLine(transform.position, Detector.nearByEnemy.transform.position, Color.red);

    }

    protected void ChaseState()
    {
        // Move the object NEAR the detected object's location.
        // Put a minimum distance between detected object and the chasing object.
        // (Detector.nearByEnemy.transform.localScale.x / 2) gets the radius of the detected object.
        // (Detector.nearByEnemy.transform.localScale.x / 2 + distanceAway) Is the minimum distance the chasing object should be away from (target's edge + distanceAway).

        dir = Detector.nearByEnemy.transform.position - transform.position;

        // If the chasing object is near enough and has bigger scale than the detected object, the object will shift to attack state.
        if (dir.magnitude < (Detector.nearByEnemy.transform.localScale.x / 2 + distanceAway) && (transform.localScale.x > Detector.nearByEnemy.transform.localScale.x))
        {
            curState = FSMStates.Attack;
            Debug.Log("Transition to attack state!");
        }

        transform.position = Vector2.MoveTowards(transform.position, Detector.nearByEnemy.transform.position, (enemySpeed / scale) * Time.deltaTime);
        Debug.Log("AI is chasing");
        Debug.DrawLine(transform.position, Detector.nearByEnemy.transform.position, Color.yellow);
    }

    protected void RunState()
    {
        transform.position = Vector2.MoveTowards(transform.position, Detector.nearByEnemy.transform.position, (-enemySpeed / scale) * Time.deltaTime);
        Debug.Log("AI is running away!");
    }

    Vector3 GenerateDestinationPoint(Vector2 DestPoint)
    {
        float x = Random.Range(-RandomPointX, RandomPointX);
        float y = Random.Range(-RandomPointY, RandomPointY);
        DestPoint = new Vector2(x, y);

        return DestPoint;
    }

    void EvaluateState()
    {
        dir = Detector.nearByEnemy.transform.position - transform.position;

        if (Detector.nearByEnemy.transform.localScale.x > transform.localScale.x)
        {
            curState = FSMStates.Run;
        }

        if (Detector.nearByEnemy.transform.localScale.x < transform.localScale.x)
        {
            curState = FSMStates.Chase;
        }
    }
    // Enemy's eat function
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            Debug.Log("Food!");
            scale += growthIncrease;
            Destroy(other.gameObject);

            //Detector.GetComponent<EnemyDetector>().detect = fa wlse;
        }

        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
        {
            if (scale > other.gameObject.transform.localScale.x)
            {
                Debug.Log("AI ate its enemy!");
                scale += other.gameObject.transform.localScale.x;
                Destroy(other.gameObject);
            }
        }
    }
}



