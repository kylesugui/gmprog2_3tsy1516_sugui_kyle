﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour {

    Vector2 velocity;

    // Smooth Time modifier
    public float smoothTime = 0.25f;
    public GameObject target;

    public Player player;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("player");
    }
    // Update is called once per frame
    void LateUpdate ()
    {
        // Zoom out camera or increase size whenever player's scale increases.
        // 5 is the minimum size.

        GetComponent<Camera>().orthographicSize = player.scale + 5; 

        transform.position = Vector2.MoveTowards(transform.position, target.transform.position, 1.0f);
        
        // smoothdamp for smooth camera        
    }
}
