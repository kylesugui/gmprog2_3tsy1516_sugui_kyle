﻿using UnityEngine;
using System.Collections;

public class EnemyDetector : MonoBehaviour
{

    public Transform nearByEnemy;
    public bool detect = false;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            nearByEnemy = other.gameObject.transform;
            detect = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            nearByEnemy = null;
        }
    }

}