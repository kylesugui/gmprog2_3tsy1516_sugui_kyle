﻿using UnityEngine;
using System.Collections;

// Make a base class for cells if more time (foods, enemies, player)
public class Player : MonoBehaviour
{
    public float scale = 1.0f;

    [SerializeField]
    float movementSpeed = 10.0f;
    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        // Get the mouse's position.
        // Move the game object towards that mouse's position.
        // Make the speed proportional to the game object's scale (by division);
        // Make cell's movement smooth when changing direction (for delay)
        transform.localScale = new Vector2(scale, scale);

        Vector2 mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = Vector2.MoveTowards(transform.position, mouseLocation, (movementSpeed / scale) * Time.deltaTime);
        // before, speed was based on scale and the distance between player and mouse (farther the faster w/ max speed).
        // now it's just based on scale.
    }
}