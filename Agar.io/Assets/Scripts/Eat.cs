﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
// Improve this
public class Eat : MonoBehaviour
{
    public Text scoreLetters;
    public int score = 0;

    [SerializeField]
    string FoodTag;

    [SerializeField]
    string EnemyTag;

    public Player player;
    public FSM enemy;

    void start()
    {
    }
    void OnTriggerEnter(Collider other)
    {        
        if (other.gameObject.tag == FoodTag)
        {
            Debug.Log("Food!");
            player.scale++;
            Destroy(other.gameObject);
            score++;
        }

        if (other.gameObject.tag == EnemyTag)
        {
            if (player.scale < enemy.scale)
            {
                Debug.Log("Enemy has eaten you!");
                Destroy(this.gameObject);
                enemy.scale += (int)player.scale;
            }
        }

        if (other.gameObject.tag == EnemyTag)
        {
            if (player.scale > enemy.scale)
            {
                Debug.Log("You ate the enemy!");
                player.scale += enemy.scale;
                Destroy(other.gameObject);

                score += (int)enemy.scale; //Add enemy's current score to yours.
            }
        }
        scoreLetters.text = "SCORE: " + score;
    }
}
